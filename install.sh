#!/bin/sh

# Urls
OSX_MAVERICKS_URL='http://131.123.42.38/lmcrs/beta/srcML-MacMavericks.dmg'
OSX_YOSEMITE_URL='http://131.123.42.38/lmcrs/beta/srcML-MacYosemite.dmg'
OSX_EL_CAPITAN_URL='http://131.123.42.38/lmcrs/beta/srcML-MacElCapitan.tar.gz'
UBUNTU1404_64_URL='http://131.123.42.38/lmcrs/beta/srcML-Ubuntu14.04-64.deb'
UBUNTU1404_32_URL='http://131.123.42.38/lmcrs/beta/srcML-Ubuntu14.04.deb'

# Hashes

OSX_MAVERICKS_MD5='7cbc01acc913de1c1fe59c41c97f9ed2'
OSX_YOSEMITE_MD5='0594618aa7116e290b82c826f8740401'
OSX_EL_CAPITAN_MD5='e79c65d23e8c0b18601e663eabb72a87'
UBUNTU1404_64_MD5='3ee7cfb7de2087aceaf529d90d3aaa44'
UBUNTU1404_32_MD5='38c790bcb22b7bd79eb900d94e0003f0'

install_osx () {
  osx_version=$1
  url="_URL"
  osx_url=$(eval "echo \$$osx_version$url")
  md5="_MD5"
  version_md5=$(eval "echo \$$osx_version$md5")
  # create tempfile
  SRCML_INSTALLFILE=$(mktemp -t srcml-$$-XXXXXXXX)
  # delete tmp file on aborts
  trap 'rm $SRCML_INSTALLFILE' 1 2 3 15

  # Download srcml and check checksums
  MD5=$(curl -s $osx_url | tee -a $SRCML_INSTALLFILE | md5 | cut -c -32)
  if [ "$MD5" = "$version_md5" ]; then
    echo 'Installing SrcML..'
    # check if we have a dmg or tar.gz file
    if file $SRCML_INSTALLFILE | grep "gzip" > /dev/null; then
      srcml_unzipped=$(mktemp -d -t srcml_unzipped-$$-XXXXXXXX)
      # delete tmp file on aborts
      trap 'rm $srcml_unzipped' 1 2 3 15
      tar -xvzf $SRCML_INSTALLFILE -C $srcml_unzipped
      echo "Copying SrcML executable, header and libraries to /usr/local/[bin,include,lib]"
      cp -r $srcml_unzipped/srcml/* /usr/local
      rm -r $srcml_unzipped
    else
      # try to mount the dmg
      echo "Installing SrcML from dmg"
      sudo hdiutil attach $SRCML_INSTALLFILE
      sudo installer -pkg /Volumes/srcML/srcML.pkg -target /
      hdiutil detach /Volumes/srcML
    fi
  else
    echo 'Aborting install, could not download SrcML from the location known to this script. Please manually install from ww.srcml.com'
  fi
  # remove tmp file
  rm $SRCML_INSTALLFILE
}

install_ubuntu () {
  ubuntu_version=$1
  url="_URL"
  ubuntu_url=$(eval "echo \$$ubuntu_version$url")
  md5="_MD5"
  version_md5=$(eval "echo \$$ubuntu_version$md5")
  # create tempfile
  SRCML_INSTALLFILE=$(mktemp -t srcml-$$-XXXXXXXX)
  # delete tmp file on aborts
  trap 'rm $SRCML_INSTALLFILE' 1 2 3 15
  # Download srcml and check checksums
  MD5=$(curl -s $ubuntu_url | tee -a $SRCML_INSTALLFILE | md5sum | cut -c -32)
  if [ "$MD5" = "$version_md5" ]; then
    echo 'Installing SrcML..'
    sudo dpkg -i $SRCML_INSTALLFILE
  else
    echo 'Aborting install, could not download SrcML from the location known to this script. Please manually install from ww.srcml.com'
  fi
  # remove tmp file
  rm $SRCML_INSTALLFILE
}

install_srcml () {
  # Check OS/Distribution and download/install
  if uname -a | grep "Darwin" > /dev/null; then
    if sw_vers -productVersion | grep "10.11" > /dev/null; then
      echo 'Detected OSX El Capitan'
      install_osx OSX_EL_CAPITAN
    elif sw_vers -productVersion | grep "10.10" > /dev/null; then
      echo 'Detected OSX Yosemite'
      install_osx OSX_YOSEMITE
    elif sw_vers -productVersion | grep "10.9" > /dev/null; then
      echo 'Detected OSX Mavericks'
      install_osx OSX_MAVERICKS
    fi
  elif uname -a | grep -i "ubuntu" > /dev/null; then
    if uname -a | grep "x86_64" > /dev/null; then
      echo 'Detected Ubuntu 64bit'
      install_ubuntu UBUNTU1404_64
    elif uname -a | grep "x86_32" > /dev/null; then
      echo 'Detected Ubuntu 32bit'
      install_ubuntu UBUNTU1404_32
    fi
  else
    echo 'Sorry, your OS is not supported'
    echo 'Supported OSX versions: 10.9 and 10.10'
    echo 'Supported Linux distributions: Ubuntu 32/64'
    echo 'Windows is not supported at this time'
  fi
}

while true; do
  read -p "Do you wish to install srcML? [Yy/Nn]" yn
  case $yn in
    [Yy]* ) install_srcml; break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done
